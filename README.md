## Useful Links
+ Brief description of everything -- `https://linuxcontainers.org/lxd/getting-started-cli/`


## Installation and Setup
+ Install using `sudo snap lxd` command.
+ Do the initial setup by running `sudo lxd init`.


## Creating new container
+ Use `lxc launch` command.
+ Syntax: `lxc launch <image-server>:<image-name> <instance-name>`
+ Use `-p <profile-name>` to specify profiles to apply to new instance.
+ Use `--vm` option to create a virtual machine.
+ Example: `lxc launch ubuntu:22.04 test -p default -p ultimate`.
+ On creation of new instance update the ownership of `$HOME` directory using `chown`.


## Notes
+ `lxc_devices.py` file is kept here for archival purpose. The `ultimate` profile takes care of all
  device mounts as well.
+ When launching / starting an instance (container), if there is `cgroup` related `ERROR` in logs,
  run `sudo umount /sys/fs/cgroup/net_cls` to see if that fixes it.

