import os
import sys


def disk_devices():
    data = {
        "host_bash_aliases": "/home/aravind/.bash_aliases",
        "host_desktop": "/home/aravind/Desktop",
        "host_documents": "/home/aravind/Documents",
        "host_downloads": "/home/aravind/Downloads",
        "host_gitconfig": "/home/aravind/.gitconfig",
        "host_gitignore_global": "/home/aravind/.gitignore-global",
        "host_home_bin": "/home/aravind/bin",
        "host_repos": "/home/aravind/repos",
        "host_ssh": "/home/aravind/.ssh",
        "host_vim": "/home/aravind/.vim",
        "host_vimrc": "/home/aravind/.vimrc",
    }
    return data


def remove_devices(cnt_name):
    cmd_pattern = "lxc config device remove {} {} --verbose"
    for k in disk_devices().keys():
        cmd = cmd_pattern.format(cnt_name, k)
        os.system(cmd)


def add_devices(cnt_name):
    cmd_pattern = "lxc config device add {} {} disk source={} path={}"
    for k, v in disk_devices().items():
        cmd = cmd_pattern.format(cnt_name, k, v, v)
        os.system(cmd)


def main():
    cnt_name = sys.argv[1]
    remove_devices(cnt_name)
    add_devices(cnt_name)


if __name__ == "__main__":
    main()


