config:
  # Make sure echo $DISPLAY outputs :0 on the host
  # Otherwise, change the value below
  environment.DISPLAY: :0
  # Makes nvidia driver from the host visible inside a container
  # You don't need this if you are not using nvidia
  # Make sure nvidia-container-runtime package is installed
  #nvidia.driver.capabilities: all
  #nvidia.runtime: "true"
  raw.idmap: both 1000 1000
  # To allow running of other containers inside this one
  security.nesting: "true"
  user.user-data: |
    #cloud-config
    users:
      - name: aravind
        sudo: ['ALL=(ALL) NOPASSWD:ALL']
        groups: sudo
        shell: /bin/bash
    package_upgrade: true
    packages:
      - x11-apps
      - mesa-utils
    locale: en_US.UTF-8
    timezone: Asia/Singapore
description: GUI+user+timezone+mounts LXD profile
devices:
  GPU:
    type: gpu
  # Unix domain socket for X11
  # If your $DISPLAY value is, say, :1, change X0 below to X1.
  X0:
    path: /tmp/.X11-unix/X0
    source: /tmp/.X11-unix/X0
    type: disk
  Xauthority:
    path: /home/aravind/.Xauthority
    source: /home/aravind/.Xauthority
    type: disk
  # file paths to mount
  host_bash_aliases:
    path: /home/aravind/.bash_aliases
    source: /home/aravind/.bash_aliases
    type: disk
  host_desktop:
    path: /home/aravind/Desktop
    source: /home/aravind/Desktop
    type: disk
  host_documents:
    path: /home/aravind/Documents
    source: /home/aravind/Documents
    type: disk
  host_downloads:
    path: /home/aravind/Downloads
    source: /home/aravind/Downloads
    type: disk
  host_gitconfig:
    path: /home/aravind/.gitconfig
    source: /home/aravind/.gitconfig
    type: disk
  host_gitignore_global:
    path: /home/aravind/.gitignore-global
    source: /home/aravind/.gitignore-global
    type: disk
  host_home_bin:
    path: /home/aravind/bin
    source: /home/aravind/bin
    type: disk
  host_repos:
    path: /home/aravind/repos
    source: /home/aravind/repos
    type: disk
  host_ssh:
    path: /home/aravind/.ssh
    source: /home/aravind/.ssh
    type: disk
  host_vim:
    path: /home/aravind/.vim
    source: /home/aravind/.vim
    type: disk
  host_vimrc:
    path: /home/aravind/.vimrc
    source: /home/aravind/.vimrc
    type: disk
name: ultimate
used_by: []
